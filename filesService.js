const {
	fileExists,
	isValidExtencion,
	addToProtectedFiles,
	checkPassword,
} = require('./helpers');

const fs = require('fs');
const path = require('path');

const dirPath = path.dirname('./files/*');
const protectedFilesJSON = fs.readFileSync('protectedFiles.json', 'utf8');

function createFile(req, res) {
	try {
		if (!req.body.content) {
			console.log("Please specify 'content' parameter");
			return res.status(400).json({
				message: "Please specify 'content' parameter",
			});
		} else if (!req.body.filename || !isValidExtencion(req.body.filename)) {
			console.log("Please specify 'filename' parameter");
			return res.status(400).json({
				message: "Please specify 'filename' parameter",
			});
		} else {
			if (req.body.password) {
				addToProtectedFiles(req);
			}
			fs.writeFile(
				`${dirPath}/${req.body.filename}`,
				req.body.content,
				(err) => {
					console.log('File created successfully');
					return res.status(200).send({ message: 'File created successfully' });
				}
			);
		}
	} catch (err) {
		console.log('Server error');
		res.status(500).send({
			message: 'Server error',
		});
	}
}

function getFiles(req, res) {
	try {
		fs.readdir(dirPath, (err, files) => {
			if (files.length === 0) {
				console.log('Client error');
				return res.status(400).send({
					message: 'Client error',
				});
			}
			console.log(`Succes, ${files}`);
			res.status(200).send({
				message: 'Success',
				files: files,
			});
		});
	} catch (err) {
		console.log('Server error');
		res.status(500).send({
			message: 'Server error',
		});
	}
}

const getFile = (req, res) => {
	try {
		if (
			fileExists(req.params.filename) &&
			!JSON.parse(protectedFilesJSON).some(
				(item) => item.file.filename === req.params.filename
			)
		) {
			const content = fs
				.readFileSync(`${dirPath}/${req.params.filename}`, 'utf8')
				.toString();
			const uploadedDate = fs.statSync(
				`${dirPath}/${req.params.filename}`
			).birthtime;
			const extension = path.extname(req.params.filename).slice(1);
			console.log(
				`Succes, content: ${content}, filename: ${
					req.params.filename
				}, extension ${path
					.extname(req.params.filename)
					.slice(1)}, uploadedDate: ${uploadedDate}`
			);
			return res.status(200).send({
				message: 'Success',
				filename: req.params.filename,
				content,
				extension,
				uploadedDate,
			});
		} else if (
			fileExists(req.params.filename) &&
			JSON.parse(protectedFilesJSON).some(
				(item) => item.file.filename === req.params.filename
			)
		) {
			checkPassword(req.params.filename, req.query.password, res);
		} else {
			console.log(`No file with '${req.params.filename}' filename found`);
			return res.status(400).send({
				message: `No file with '${req.params.filename}' filename found`,
			});
		}
	} catch (err) {
		console.log('Server error');
		res.status(500).send({
			message: 'Server error',
		});
	}
};

function editFile(req, res) {
	try {
		if (fileExists(req.params.filename) && !!req.body.content) {
			return fs.writeFile(
				`${dirPath}/${req.params.filename}`,
				req.body.content,
				'utf8',
				(err) => {
					console.log('File edited successfully');
					res.status(200).send({ message: 'File edited successfully' });
				}
			);
		} else {
			console.log(
				`No file with '${req.params.filename}' filename found or 'content' is invalid`
			);
			return res.status(400).send({
				message: `No file with '${req.params.filename}' filename found or 'content' is invalid`,
			});
		}
	} catch (err) {
		console.log('Server error');
		res.status(500).send({
			message: 'Server error',
		});
	}
}

function deleteFile(req, res) {
	try {
		if (fileExists(req.params.filename)) {
			fs.unlinkSync(`${dirPath}/${req.params.filename}`);
			console.log(`File ${req.params.filename} deleted`);
			res.status(200).send({
				message: `File ${req.params.filename} deleted`,
			});
		} else {
			console.log(`No file with '${req.params.filename}' filename found`);
			return res.status(400).send({
				message: `No file with '${req.params.filename}' filename found`,
			});
		}
	} catch (err) {
		console.log('Server error');
		res.status(500).send({
			message: 'Server error',
		});
	}
}

module.exports = {
	createFile,
	getFiles,
	getFile,
	editFile,
	deleteFile,
};
