const fs = require('fs');
const path = require('path');

const dirPath = path.dirname('./files/*');
const protectedFilesJSON = fs.readFileSync('protectedFiles.json', 'utf8');

function fileExists(file) {
	const files = fs.readdirSync(dirPath);
	return files.includes(file) ? true : false;
}

function isValidExtencion(filename) {
	const fileExtensions = new RegExp(
		/([a-zA-Z0-9\s_\\.\-\(\):])+(.log|.txt|.json|.yaml|.xml|.js)$/i
	);
	return fileExtensions.test(filename);
}

function addToProtectedFiles(req) {
	const { filename, password } = req.body;
	const file = { file: { filename, password } };
	const previousData = JSON.parse(
		fs.readFileSync('protectedFiles.json', 'utf8')
	);
	previousData.push(file);
	fs.writeFile(
		'protectedFiles.json',
		JSON.stringify(previousData),
		'utf8',
		() => {
			console.log('Added to protected files');
		}
	);
}

function checkPassword(filename, password, res) {
	if (
		JSON.parse(protectedFilesJSON).some(
			(item) =>
				item.file.filename === filename && item.file.password === password
		)
	) {
		const content = fs
			.readFileSync(`${dirPath}/${filename}`)
			.toString();
		const uploadedDate = fs.statSync(
			`${dirPath}/${filename}`
		).birthtime;
		const extension = path.extname(filename).slice(1);
		return res.status(200).send({
			message: 'Success',
			filename: filename,
			content,
			extension,
			uploadedDate,
		});
	} else {
		return res.status(400).send({
			message: 'This is protected file, write correct password',
		});
	}
}

module.exports = {
	fileExists,
	isValidExtencion,
	addToProtectedFiles,
	checkPassword,
};
